<?php

namespace app\Http\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Camera extends Eloquent{

	protected $table = 'camera';
	protected $fillable = array('nom', 'adresseip', 'idUser');
}