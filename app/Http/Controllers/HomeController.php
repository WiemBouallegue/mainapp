<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function showLogin()
{
    // show the form
    return view('login');
}

public function doLogin()
{
	 $userdata = array(
        'email'     => Input::get('email'),
        'password'  => Input::get('password'));
         if (Auth::attempt($userdata)) {
         	$user = DB::table('users')->where('email', Input::get('email'))->first();
         	$name=$user->username;
         	$id=$user->id;
            $cameraData=[];
         	return redirect()->Route('dashboard', ['username'=>$name, 'id'=>$id])->with('cameraData' ,$cameraData)->with('messagelist', 'none')->with('message', 'none')->with('messageBox', 'none');
         }
         else {        

        // validation not successful, send back to form 
        return Redirect::to('login');

    }

    
}

public function showSignup()
{
    
    return view('signup')->with('emailexist', '')	;
}

public function doSignup(){

	$data= array(
		'username'=>Input::get('username'),
		'email'=> Input::get('email'),
		'password'=> bcrypt(Input::get('password')));
	$user = DB::table('users')->where('email', Input::get('email'))->first();
	if(empty($user)){
		DB::table('users')->insert($data);
	echo "success";
		
	}
	else{

		 return view('signup')->with('emailexist', 'this email is already been used');
	}

}
}
