<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('login', 'HomeController@showLogin');
Route::post('login', 'HomeController@doLogin');
Route::get('SignUp', 'HomeController@showSignup');
Route::post('SignUp', 'HomeController@doSignup');
//Route::resource('emailexist','HomeController');
Route::post('dashboard/{username}/{iddd}/add',  'CameraController@addCamera')->name("dashboard");
Route::get('dashboard/{username}/{id}/add', 'CameraController@showCamera');
//Route::get('dashboard/{username}/{id}/delete', 'CameraController@deleteCamera');

Route::get('dashboard/{username}/{id}', 'CameraController@showDashboard')->name("dashboard");
Route::get('dashboard/{username}/{id}/liste', 'CameraController@listCamera')->name("dashboard");

Route::get('dashboard/{username}/{id}/{id2}', [
        'uses' => 'CameraController@deleteCamera',
        'as'   => 'del'
      ]);

Route::get('send','emailController@sendMail');
Route::get('test', 'smsController@smsSend');





