<!DOCTYPE html>
<html lang="en">
<head>
  <title>Login</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
  	.box{
  		height: 400px;
  		width: 400px;
  		margin: auto;
  		margin-top: 130px;
  	}
  	#submitButton{
		
		padding-top: 150px;
		margin-left: 180px;
  	}

  </style>
</head>
<body>

<nav class="navbar navbar-inverse ">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Surveillance</a>
    </div>
    <div dir="rtl">
    <ul class="nav navbar-nav ml-auto">
      <li class="active"><a href="/login">Login</a></li>
      <li><a href="/SignUp">Sign up</a></li>
    </ul>
    </div>
  </div>
</nav>
  
<div class="box">
 <fieldset>
 	<form method="post">
  {{ csrf_field() }}
  <div class="form-group">
    <div class="col-xs-10">
    <label for="formGroupExampleInput">E-Mail Address</label>
    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="example@gmail.com" name="email" type="email">
    </div>
  </div>
  <div class="form-group">
  <div class="col-xs-10">
    <label for="formGroupExampleInput2">Password</label>
    <input type="password" class="form-control" id="formGroupExampleInput2" placeholder="Password" name="password">
  </div>
  </div>
  <div id="submitButton">
  <button type="reset" class="btn btn-m active" aria-pressed="true">Cancel</button>
  <button class="btn 	 btn-m active"  type="submit">Login </button>
	</div>

</form>
 </fieldset>
</div>

</body>
</html>
