<!DOCTYPE html>
<html lang="en">
<head>
  <title>Sign up</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
  	.box{
  		height: 400px;
  		width: 400px;
  		margin: auto;
  		margin-top: 60px;
     
  	}
  	#submitButton{
		
		padding-top: 280px;
		margin-left: 180px;
  	}

    input[type=text]{
      margin-bottom:15px;
    }

    input[type=password]{
      margin-bottom:15px;
    }
    

  </style>
</head>
<body>

<nav class="navbar navbar-inverse ">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Surveillance</a>
    </div>
    <div dir="rtl">
    <ul class="nav navbar-nav ml-auto">
      <li ><a href="/login">Login</a></li>
      <li class="active"><a href="/SignUp">Sign up</a></li>
    </ul>
    </div>
  </div>
</nav>
  
<div class="box">
 <fieldset>
 	<form name="frmSignup" method="post" onsubmit="return validateSignup()" >
  {{ csrf_field() }}
  <div class="form-group">
    <div class="col-xs-10">
    <label for="username">User Name:</label>
    <input type="text" class="form-control" id="username" placeholder="Wiem" name="username" onblur='checkUserName();' oninput="validateUser();">
    <span id='messageUser'></span>
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-10">
    <label for="formGroupExampleInput">E-Mail Address:</label>
    <input type="text" class="form-control" id="email" placeholder="example@gmail.com" name="email" type="email" oninput="validate();" onblur="validateEmptyEmail()">
   
    <span id='messageEmail' style="color: red;">{{ $emailexist }}</span>


    </div>
    
  </div>
  <div class="form-group">
  <div class="col-xs-10">
    <label for="password">Password:</label>
    <input type="password" class="form-control" id="password" placeholder="Password" name="password" onkeyup='check();' onblur='deleteMessage();' oninput="tapePwd();">
    <span id='message1'></span>
  </div>
  </div>
  <div class="form-group">
  <div class="col-xs-10">
    <label for="passwordConfirm">Password Confirm:</label>
    
    <input type="password" class="form-control" id="passwordConfirm" placeholder="Password" name="passwordConfirm" onkeyup='check();' style="display:inline;" onblur='deleteMessage2();' oninput="tapePwd();">
     <span id='message'></span>
    </div>
  </div>
 

  <div id="submitButton">
  <button type="reset" class="btn btn-m active" aria-pressed="true">Cancel</button>
  <button id="submitB" class="btn 	 btn-m active"  type="submit">Sign up </button>
	</div>




</form>
 </fieldset>
</div>

<script>

var check = function() {
  if((document.getElementById('password').value !="")&&(document.getElementById('passwordConfirm').value !="")){
  if (document.getElementById('password').value ==
    document.getElementById('passwordConfirm').value) {
    document.getElementById('message').style.color = 'green';
    document.getElementById('message').innerHTML = 'matching';
  
  } else {
    document.getElementById('message').style.color = 'red';
    document.getElementById('message').innerHTML = 'not matching';
    
  }
}}

var checkUserName = function() {
  if((document.getElementById('username').value ==null)||(document.getElementById('username').value ==""))
  {
    document.getElementById('messageUser').style.color = 'red';
    document.getElementById('messageUser').innerHTML = 'User name is required';
   
  
}

}

var validateUser=function(){
  if(document.getElementById('username').value !=""){
     document.getElementById('messageUser').innerHTML = '';
  }

}

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function validate() {
  
  var email = document.getElementById('email').value;
  
 if (validateEmail(email)) {

    document.getElementById('messageEmail').innerHTML = ' ';
    
  } else {
    document.getElementById('messageEmail').style.color = 'red';
    document.getElementById('messageEmail').innerHTML = 'invalid Email';
  }
  return false;
}

var validateEmptyEmail=function(){
  if ((document.getElementById('email').value =="")||(document.getElementById('email').value ==null)){
    document.getElementById('messageEmail').style.color = 'red';
    document.getElementById('messageEmail').innerHTML = 'Email is required';

  }
}

var deleteMessage =function(){
  if(document.getElementById('message').innerHTML=='matching'){
    document.getElementById('message').innerHTML = '';
    
  }

   if((document.getElementById('password').value ==null)||(document.getElementById('password').value ==""))
  {
    document.getElementById('message1').style.color = 'red';
    document.getElementById('message1').innerHTML = 'Password is required';
   
  
}


  
}

var deleteMessage2 =function(){
  if(document.getElementById('message').innerHTML=='matching'){
    document.getElementById('message').innerHTML = '';
    
  }

   if((document.getElementById('passwordConfirm').value ==null)||(document.getElementById('passwordConfirm').value ==""))
  {
    document.getElementById('message').style.color = 'red';
    document.getElementById('message').innerHTML = 'Confirming password is required';
   
  
}

  
}

 function validateSignup(){
  
  var exist=true;
    if((document.getElementById('messageUser').innerHTML == 'User name is required')||(document.getElementById('messageEmail').innerHTML == 'Email is required')||(document.getElementById('messageEmail').innerHTML == 'invalid Email')||(document.getElementById('message').innerHTML == 'not matching')||(document.getElementById('message1').innerHTML == 'Password is required')||(document.getElementById('message').innerHTML == 'Confirming password is required'))
      exist=false;

    if((document.getElementById('username').value ==null)||(document.getElementById('username').value ==""))
  {
    document.getElementById('messageUser').style.color = 'red';
    document.getElementById('messageUser').innerHTML = 'User name is required';
   exist=false;
  
}

if ((document.getElementById('email').value =="")||(document.getElementById('email').value ==null)){
    document.getElementById('messageEmail').style.color = 'red';
    document.getElementById('messageEmail').innerHTML = 'Email is required';

  }

if((document.getElementById('password').value ==null)||(document.getElementById('password').value ==""))
  {
    document.getElementById('message1').style.color = 'red';
    document.getElementById('message1').innerHTML = 'Password is required';
   
  
}


 if((document.getElementById('passwordConfirm').value ==null)||(document.getElementById('passwordConfirm').value ==""))
  {
    document.getElementById('message').style.color = 'red';
    document.getElementById('message').innerHTML = 'Confirming password is required';
   
  
}




    return exist;
  /*else if(validateEmail(document.getElementById('email').value)&&(document.getElementById('email').value!=null)&&(document.getElementById('email').value!="")&&((document.getElementById('password').value ==
    document.getElementById('passwordConfirm').value))&&(document.getElementById('password').value!=null)&&(document.getElementById('passwordConfirm').value!="")){

    
}*/

}

var tapePwd=function() {
  // body...
  if((document.getElementById('password').value !=""))
  {
    
    document.getElementById('message1').innerHTML = '';
   
  
}

if((document.getElementById('passwordConfirm').value !=""))
  {
    
    document.getElementById('message').innerHTML = '';
   
  
}
}
</script>
</body>
</html>
